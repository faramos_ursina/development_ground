from ursina import *

app = Ursina()

# =================

class Game:
    def __init__(self):
        self.list_of_game_objects = []

    def add_object(self, object):
        self.list_of_game_objects.append(object)
        return object

    def mass_update(self):
        for object in self.list_of_game_objects:
            object.update()

# =================

class GameObject:
    def __init__(self, selection_color=color.rgb(1, 1, 1, 0.3), scale=1, position=(0,0), text="GameObject", collider=True, debug=False, selection_circle_visible=False, selection_color_visible=False, text_visible=True):

        self.selection_circle = Entity(model=Circle(64, mode='line', radius=scale, thickness=3), color=color.white, position=position, visible_self=selection_circle_visible)
        self.selection_color =  Entity(parent=self.selection_circle, model=Circle(64, mode='ngon', radius=scale, thickness=3), color=selection_color, visible_self=selection_color_visible)
        self.text = Text(parent=self.selection_circle, text=text, scale=10*scale, position=(0, (-scale -(0.2*scale) )  ), origin=(0,0), visible_self=text_visible)

        if collider:
            print("> SETTING COLLIDER")
            #self.selection_color.collider = MeshCollider(self.selection_color, mesh=self.selection_color.model)
            self.selection_color.collider = SphereCollider(self.selection_color, radius=scale)
            # DEBUG:
            #self.selection_color.collider.visible = True
            #print(self.selection_color.collider)

        if debug:
            print("> RUNNING DEBUG")
            self.selection_color.on_mouse_enter = lambda: print("> MOUSE CURSOR OVER: "+text)

        # for now just use green square as a placeholder for the actual object
        self.object_itself = Entity(parent=self.selection_circle, model='quad', texture="./player.png", color=color.green, scale=(scale, scale))

        # Object movement and related stuff
        self.velocity = Vec2(0,0)
        self.acceleration = 0
        self.mass = 1
        self.force = Vec2(0,0)

        self.rotation = 0

    def update(self):
        self.acceleration = self.force / self.mass
        self.velocity += self.acceleration * time.dt
        self.selection_circle.x += self.velocity.x * time.dt
        self.selection_circle.y += self.velocity.y * time.dt

        self.object_itself.rotation_z += self.rotation * time.dt
        self.rotation = 0

# =================

game = Game()
player = game.add_object( GameObject(scale=1, position=(1,1), text="Player", debug=True, selection_circle_visible=True, selection_color_visible=True) )

asteroid = game.add_object( GameObject(scale=1, position=(-1,-1), text="Asteroid", debug=True, selection_circle_visible=True, selection_color_visible=True) )

def update():
    # Check for key presses
    if held_keys['right arrow']:
        player.force = Vec2(5, 0)
    elif held_keys['left arrow']:
        player.force = Vec2(-5, 0)
    elif held_keys['up arrow']:
        player.force = Vec2(0, 5)
    elif held_keys['down arrow']:
        player.force = Vec2(0, -5)
    elif held_keys['q']:
        player.rotation = 50
    elif held_keys['e']:
        player.rotation = -50
    else:
        player.force = Vec2(0, 0)

    if player.selection_color.intersects(asteroid.selection_color):
        print("!! COLLISION !!")
        v1_final = ((player.mass - asteroid.mass) * player.velocity + 2 * asteroid.mass * asteroid.velocity) / (player.mass + asteroid.mass)
        v2_final = ((asteroid.mass - player.mass) * asteroid.velocity + 2 * player.mass * player.velocity) / (player.mass + asteroid.mass)

        # Set the new velocities after the collision
        player.velocity = v1_final
        asteroid.velocity = v2_final

    game.mass_update()

# =================

#ed = EditorCamera(rotation_speed = 200, panning_speed=200)
app.run()

# =================

# To-DO:
#   render image on top of green square
#   function for collisions with other objects
#   asteroids
#
#   shields ? engines ? thrusters ?

# N /home/faramos/.local/lib/python3.11/site-packages/ursina/models/procedural/circle.py
# https://www.ursinaengine.org/entity_basics.html
# https://www.ursinaengine.org/introduction_tutorial.html
# https://www.ursinaengine.org/platformer_tutorial.html
# https://www.ursinaengine.org/api_reference.html#Ursina
# https://github.com/pokepetter/ursina/blob/master/ursina/camera.py
# https://github.com/pokepetter/ursina/blob/master/samples/cheap_physics.py
#
# https://gitlab.com/faramos_ursina/development_ground
