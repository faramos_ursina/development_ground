from ursina import *

app = Ursina()

# Create a white ball representing the object
ball = Entity(model='sphere', color=color.white, scale=(1, 1, 1), position=(0, 0, 0))

# Initial velocity and mass
velocity = Vec3(0, 0, 0)  # Initial velocity vector
mass = 10.0  # Mass of the object

# Initial force and acceleration
force = Vec3(0, 0, 0)  # Initial force vector
acceleration = Vec3(0, 0, 0)  # Initial acceleration vector

# Force applied when an arrow key is pressed
#applied_force = Vec3(5, 0, 0)  # Force applied in the x-direction

def update():
    global velocity, force, acceleration

    # Check for key presses
    if held_keys['right arrow']:
        force = Vec3(5, 0, 0)
    elif held_keys['left arrow']:
        force = Vec3(-5, 0, 0)
    elif held_keys['up arrow']:
        force = Vec3(0, 5, 0)
    elif held_keys['down arrow']:
        force = Vec3(0, -5, 0)
    else:
        force = Vec3(0, 0, 0)  # No force when the key is not pressed

    # Calculate acceleration using Newton's second law: F = m * a
    acceleration = force / mass

    # Update velocity using the equation: v = u + at
    velocity += acceleration * time.dt

    # Move the ball based on the updated velocity
    ball.x += velocity.x * time.dt
    ball.y += velocity.y * time.dt
    ball.z += velocity.z * time.dt

app.run()
