from ursina import *

app = Ursina()

# Create a red square
red_square = Entity(model='quad', color=color.red, scale=(1, 1), position=(-5, 0))

# Initial velocity and acceleration
velocity = 0
acceleration = 2.0

def update():
    global velocity

    # Update velocity based on acceleration
    velocity += acceleration * time.dt

    # Move the square horizontally based on the updated velocity
    red_square.x += velocity * time.dt

app.run()
