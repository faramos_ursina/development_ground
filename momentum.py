from ursina import *

app = Ursina()

# Create a white ball representing the object
ball = Entity(model='sphere', color=color.white, scale=(1, 1, 1), position=(0, 0, 0))

# Initial velocity and mass
velocity = Vec3(0, 0, 0)  # Initial velocity vector
mass = 1.0  # Mass of the object

# Applying force (change in velocity per second)
force = Vec3(2, 0, 0)  # Force applied in the x-direction

def update():
    global velocity

    # Calculate acceleration using Newton's second law: F = m * a
    acceleration = force / mass

    # Update velocity using the equation: v = u + at
    velocity += acceleration * time.dt

    # Move the ball based on the updated velocity
    ball.x += velocity.x * time.dt
    ball.y += velocity.y * time.dt
    ball.z += velocity.z * time.dt

app.run()
