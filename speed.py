from ursina import *

app = Ursina()

# Create a green square
green_square = Entity(model='quad', color=color.green, scale=(1, 1), position=(0, 0))

# Define the constant speed
speed = 2.0

def update():
    # Move the square to the right with constant speed
    green_square.x += speed * time.dt

app.run()
